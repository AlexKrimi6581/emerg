package com.hatsker.alex.emergency;

import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.hatsker.alex.emergency.adapters.CustomAdapter;
import com.hatsker.alex.emergency.models.DataModel;

import java.util.ArrayList;
import java.util.List;

//import com.hatsker.alex.emergency.adapters.TableAdapter;

public class GraphFragment extends Fragment{

    ArrayList<DataModel> dataModels;
    private int[] yData = {3,1,1};
    private String[] xData = {"INFO", "WARN", "FAIL"};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){
        View rootView = inflater.inflate(R.layout.fragment_graph, container, false);

        PieChart chart = rootView.findViewById(R.id.chart);

//
        dataModels = new ArrayList<>();

        dataModels.add(new DataModel("INFO", "Post created", "30.05/02:05", "twitterPostApp"));
        dataModels.add(new DataModel("INFO", "Post posted", "30.05/02:05", "twitterPostApp"));
        dataModels.add(new DataModel("WARN", "Post is too big", "30.05/02:05", "twitterPostApp"));
        dataModels.add(new DataModel("INFO", "Post generating session start", "30.05/02:05", "twitterPostApp"));
        dataModels.add(new DataModel("FAIL", "Unable to post empty object", "30.05/02:05", "twitterPostApp"));


        List<Entry> entries = new ArrayList<Entry>();


//        chart.setDescription("Total amount of records by type");
        chart.setRotationEnabled(true);
        chart.setHoleRadius(50f);
        chart.setTransparentCircleAlpha(0);
        chart.setCenterText("");
        chart.setCenterTextSize(14);
        chart.setDrawEntryLabels(true);

        ArrayList<PieEntry> yEntry = new ArrayList<>();
        ArrayList<String> xEntry = new ArrayList<>();

        for (int i=0; i < yData.length; ++i){
            yEntry.add(new PieEntry(yData[i],i));
        }

        for (int i=0; i < xData.length; ++i){
            xEntry.add(xData[i]);
        }

        ArrayList<Integer> colors = new ArrayList<>();
        colors.add(Color.BLUE);
        colors.add(Color.YELLOW);
        colors.add(Color.RED);



        PieDataSet pieDataSet = new PieDataSet(yEntry, "log records types");
        pieDataSet.setColors(colors);

        PieData pieData = new PieData(pieDataSet);
        chart.setData(pieData);
        chart.invalidate();

//        for (YourData data : dataObjects) {
//
//            // turn your data into Entry objects
//            entries.add(new Entry(data.getValueX(), data.getValueY()));
//        }
//
//        LineDataSet dataSet = new LineDataSet(entries, "Label"); // add entries to dataset
//        dataSet.setColor(...);
//        dataSet.setValueTextColor(...); // styling, ...
//
//        String[] sample = {"something 1", "something 2", "something 3", "something 4"};
////        this.ladapter = new TableAdapter(this, sample);
//        this.lv = (ListView) getView().findViewById(R.id.status_and_something);
//
//        this.lv.setAdapter(ladapter);

        return rootView;

    }

    public void addDataSet(PieChart chart){

    }
}

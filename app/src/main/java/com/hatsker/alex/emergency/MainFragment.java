package com.hatsker.alex.emergency;

import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.hatsker.alex.emergency.adapters.CustomAdapter;
import com.hatsker.alex.emergency.models.DataModel;

import java.util.ArrayList;

public class MainFragment extends Fragment {

    private ListView listLogs;
    //    private ListAdapter ladapter;
    ArrayList<DataModel> dataModels;
    private static CustomAdapter adapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);


        listLogs = rootView.findViewById(R.id.all_available_statuses);


        dataModels = new ArrayList<>();

        dataModels.add(new DataModel("INFO", "Post created", "30.05/02:05", "twitterPostApp"));
        dataModels.add(new DataModel("INFO", "Post posted", "30.05/02:05", "twitterPostApp"));
        dataModels.add(new DataModel("WARN", "Post is too big", "30.05/02:05", "twitterPostApp"));
        dataModels.add(new DataModel("INFO", "Post generating session start", "30.05/02:05", "twitterPostApp"));
        dataModels.add(new DataModel("FAIL", "Unable to post empty object", "30.05/02:05", "twitterPostApp"));

        adapter = new CustomAdapter(dataModels, getActivity().getApplicationContext());

        listLogs.setAdapter(adapter);
//        listLogs.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//
//                DataModel dataModel= dataModels.get(position);
//
//                Snackbar.make(view, dataModel.getDetails()+"\n"+dataModel.getLogTime()+" Application: "+dataModel.getSource(), Snackbar.LENGTH_LONG)
//                        .setAction("No action", null).show();
//            }
//        });

        return rootView;

    }

}

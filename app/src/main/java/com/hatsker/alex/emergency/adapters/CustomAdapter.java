package com.hatsker.alex.emergency.adapters;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.hatsker.alex.emergency.R;
import com.hatsker.alex.emergency.models.DataModel;

import java.util.ArrayList;

public class CustomAdapter extends ArrayAdapter<DataModel> implements View.OnClickListener {
    private ArrayList<DataModel> dataSet;
    Context mContext;

    // View lookup cache
    private static class ViewHolder {
        TextView txtLogType;
        TextView txtDetails;
        TextView txtTime;
    }

    public CustomAdapter(ArrayList<DataModel> data, Context context) {
        super(context, R.layout.log_list_item, data);
        this.dataSet = data;
        this.mContext = context;

    }

    @Override
    public void onClick(View v) {

        int position = (Integer) v.getTag();
        Object object = getItem(position);
        DataModel dataModel = (DataModel) object;

        switch (v.getId()) {
            case R.id.text_details:
                Snackbar.make(v, "Release date1111111111 " + dataModel.getDetails(), Snackbar.LENGTH_LONG)
                        .setAction("No action111111", null).show();
                break;
        }
    }

    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        DataModel dataModel = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.log_list_item, parent, false);
            Log.d(convertView.toString(), "check if it is null");
            if (convertView == null) {
                LayoutInflater new_inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = new_inflater.inflate(R.layout.log_list_item, parent, false);
            }
            viewHolder.txtLogType = (TextView) convertView.findViewById(R.id.text_log_type);
            viewHolder.txtDetails = (TextView) convertView.findViewById(R.id.text_details);
            viewHolder.txtTime = (TextView) convertView.findViewById(R.id.text_time);

            result = convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result = convertView;
        }

        Animation animation = AnimationUtils.loadAnimation(mContext, (position > lastPosition) ? R.anim.bottom_up : R.anim.top_down);
        result.startAnimation(animation);
        lastPosition = position;

        viewHolder.txtLogType.setText(dataModel.getLogType());
        switch (dataModel.getLogType()) {
            case "INFO": {
                viewHolder.txtLogType.setBackgroundColor(mContext.getResources().getColor(R.color.blue_400));
                break;
            }
            case "WARN": {
                viewHolder.txtLogType.setBackgroundColor(mContext.getResources().getColor(R.color.orange_400));
                break;
            }
            case "FAIL": {
                viewHolder.txtLogType.setBackgroundColor(mContext.getResources().getColor(R.color.red_400));
                break;
            }
        }
        viewHolder.txtDetails.setText(dataModel.getDetails());
        viewHolder.txtTime.setText(dataModel.getLogTime());
//        viewHolder.info.setOnClickListener(this);
//        viewHolder.info.setTag(position);
        // Return the completed view to render on screen
        return convertView;
    }
}

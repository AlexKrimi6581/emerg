package com.hatsker.alex.emergency.models;

public class DataModel {

    String details;
    String type;
    String source;
    String time;

    public DataModel(String type, String details, String time, String source ) {
        this.details=details;
        this.type=type;
        this.source=source;
        this.time=time;

    }

    public String getDetails() {
        return details;
    }

    public String getLogType() {
        return type;
    }

    public String getSource() {
        return source;
    }

    public String getLogTime() {
        return time;
    }

}